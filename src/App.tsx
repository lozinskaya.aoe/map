import { ApplicationsModule } from './component/applicationsModule/applicationsModule';
import { RouteMap } from './component/routeMap/routeMap';
import styles from './App.module.css';



function App() {
    return (

        <div className={styles.App}>
            <ApplicationsModule />
            <RouteMap />
        </div>

    );
}

export default App;
