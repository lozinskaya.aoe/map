


import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import moduleApplicationTransportationReducer from './slice';


export const store = configureStore({
  reducer: {
    moduleApplicationTransportation: moduleApplicationTransportationReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
