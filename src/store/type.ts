import { LatLngTuple } from "leaflet";

export type ApplicationType = {
    id: number,
    shippingPoint: PointType,
    deliveryPoint:  PointType,
}

export type PointType = {
    name: string,
    point: LatLngTuple,
}

export type PointListType = Array<PointType>;

export type ApplicationListType = Array<ApplicationType>;