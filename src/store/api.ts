import { PointListType, ApplicationListType } from "./type";

const shippingPointList = [

    {
        name: "точка A",
        point: [56.84097, 60.61593]
    },
    {
        name: "точка B",
        point: [56.8282, 60.59943]
    },
    {
        name: "точка C",
        point: [56.82529, 60.61722]
    },

];

export function getShippingPointList() {
    return new Promise<PointListType>(ok => setTimeout(() => ok(shippingPointList as PointListType), 0))
}

const deliveryPointList = [

    {
        name: "точка J",
        point: [56.81633, 60.61184]
    },
    {
        name: "точка F",
        point: [56.80521, 60.58267]
    },
    {
        name: "точка E",
        point: [56.82666, 60.54777]
    },

];

export function getDeliveryPointList() {

    return new Promise<PointListType>(ok => setTimeout(() => ok(deliveryPointList as PointListType), 0))
}


const applications = [
    {
        id: 1432,
        shippingPoint: {
            name: "точка A",
            point: [56.84097, 60.61593]
        },
        deliveryPoint: {
            name: "точка J",
            point: [56.81633, 60.61184]
        }
    },
    {
        id: 4634,
        shippingPoint: {
            name: "точка B",
            point: [56.8282, 60.59943]
        },
        deliveryPoint: {
            name: "точка F",
            point: [56.80521, 60.58267]
        },
    },
    {
        id: 435,
        shippingPoint: {
            name: "точка C",
            point: [56.82529, 60.61722]
        },
        deliveryPoint: {
            name: "точка E",
            point: [56.82666, 60.54777]
        },
    },
];

export function getApplicationCollection() {
    return new Promise<ApplicationListType>(ok => setTimeout(() => ok(applications as ApplicationListType), 0));
}

