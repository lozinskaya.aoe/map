import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ApplicationType, PointType, PointListType, ApplicationListType } from "./type";

import { getApplicationCollection, getDeliveryPointList, getShippingPointList } from "./api";
import { AppThunk } from "./store";


export interface moduleRequestsTransportationState {
    applicationsCollection: ApplicationListType,
    selectedApplication: ApplicationType | null,
    shippingPointList: PointListType,
    deliveryPointList: PointListType,
}

const initialState: moduleRequestsTransportationState = {
    applicationsCollection: [],
    selectedApplication: null,
    shippingPointList: [],
    deliveryPointList: [],
};


export const moduleApplicationTransportationSlice = createSlice({
    name: 'moduleApplicationTransportation',
    initialState,

    reducers: {
        updateApplicationsCollection: (state, action) => {
            state.applicationsCollection = [...action.payload];
        },

        updateShippingPointList: (state, action) => {
            state.shippingPointList = action.payload;
        },

        updateDeliveryPointList: (state, action) => {
            state.deliveryPointList = action.payload;
        },

        updateShippingPointInApplication: (state, action: PayloadAction<{ applicationId: number, pointName: string, select: PointType }>) => {
            const newPoint = [...state.shippingPointList.filter(point => point.name === action.payload.pointName)];
            state.applicationsCollection.forEach(application => {
                if (application.id === action.payload.applicationId) {
                    application.shippingPoint = newPoint[0];
                }
            });

            state.selectedApplication = {
                id: action.payload.applicationId,
                shippingPoint: newPoint[0],
                deliveryPoint: action.payload.select,
            }
        },

        updateDeliveryPointInApplication: (state, action: PayloadAction<{ applicationId: number, pointName: string, select: PointType }>) => {
            const newPoint = state.deliveryPointList.filter(point => point.name === action.payload.pointName);
            state.applicationsCollection.forEach(application => {
                if (application.id === action.payload.applicationId) {
                    application.deliveryPoint = newPoint[0];
                }
            });

            state.selectedApplication = {
                id: action.payload.applicationId,
                shippingPoint: action.payload.select,
                deliveryPoint: newPoint[0] ,
            }
        },

        updateSelectedApplication: (state, action: PayloadAction<ApplicationType | null>) => {
            state.selectedApplication = action.payload;
        },

    }
});

export const {
    updateApplicationsCollection,
    updateShippingPointList,
    updateDeliveryPointList,
    updateShippingPointInApplication,
    updateDeliveryPointInApplication,
    updateSelectedApplication,
} = moduleApplicationTransportationSlice.actions;

export const fetchCustomer = (): AppThunk => (
    dispatch,
) => {
    getApplicationCollection().then(result => dispatch(updateApplicationsCollection(result)));
    getShippingPointList().then(result => dispatch(updateShippingPointList(result)));
    getDeliveryPointList().then(result => dispatch(updateDeliveryPointList(result)));
}

export default moduleApplicationTransportationSlice.reducer;

