
import { MapContainer, Marker, Polyline, Popup, TileLayer } from 'react-leaflet';
import { useAppSelector } from '../../store/hooks';
import delivery from './delivery.png';
import shipping from './shipping.png';
import L from 'leaflet';
import { createControlComponent } from "@react-leaflet/core";
import "leaflet-routing-machine";


const deliveryPointIcon = new L.Icon({
    iconUrl: delivery,
    iconSize: [30, 30]
})

const shippingPointIcon = new L.Icon({
    iconUrl: shipping,
    iconSize: [30, 30]
})


export function RouteMap() {
    const selectedApplication = useAppSelector(state => state.moduleApplicationTransportation.selectedApplication);

    let Rout = createControlComponent(() => {
        return L.Routing.control({
            waypoints: selectedApplication ? [
                L.latLng(selectedApplication.shippingPoint.point),
                L.latLng(selectedApplication.deliveryPoint.point)
            ] : [],


        })
    });


    if (selectedApplication) {
        const line = L.polyline([selectedApplication.shippingPoint.point, selectedApplication.deliveryPoint.point], {
            color: 'red',
            weight: 3,
            opacity: 0.5,
            smoothFactor: 1
        });
    }

    return (
        <MapContainer
            center={[56.85, 60.61]}
            zoom={10}
            style={{ 
                height: "800px", 
                width: "800px",  
                margin: "20px",
                borderRadius: "5px",
                boxShadow: "1px 1px 2px 2px rgba(102, 102, 102, 0.753)",
                minWidth: "400px"
                }} >
            <TileLayer
                attribution='&copy; <a href="https://c.tile.thunderforest.com/cycle/15/19810/10246.png?apikey=6170aad10dfd42a38d4d8c709a536f38">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />

            {selectedApplication ?

                <><Marker
                    position={selectedApplication.shippingPoint.point}
                    icon={shippingPointIcon}>
                    <Popup>
                        {selectedApplication.shippingPoint.name}
                    </Popup>
                </Marker>

                    <Rout />

                    <Marker
                        position={selectedApplication.deliveryPoint.point}
                        icon={deliveryPointIcon}>
                        <Popup>
                            {selectedApplication.deliveryPoint.name}
                        </Popup>
                    </Marker></>

                : null

            }

        </MapContainer>
    )
};


