import { useEffect } from 'react';
import { useAppDispatch, useAppSelector } from '../../store/hooks'
import { fetchCustomer } from '../../store/slice';
import { Application } from './application/application'
import styles from './applicationsModule.module.css'


export function ApplicationsModule() {
    const applicationsCollection = useAppSelector(state => state.moduleApplicationTransportation.applicationsCollection);
    const dispatch = useAppDispatch();

    useEffect(() => {
        dispatch(fetchCustomer());
    }, []);


    return (
        <div className={styles.applicationsModule}>
            {applicationsCollection.map(application => {
                return <Application key={application.id} id={application.id} shippingPoint={application.shippingPoint} deliveryPoint={application.deliveryPoint} />
            })}
        </div>
    )
}