import { Select } from 'antd';
import { Typography } from 'antd';
import { useAppDispatch, useAppSelector } from '../../../store/hooks'
import { updateShippingPointInApplication, updateDeliveryPointInApplication, updateSelectedApplication } from '../../../store/slice';
import 'antd/dist/antd.css';
import styles from './application.module.css';
import { ApplicationType, PointType } from '../../../store/type';
import { useState } from 'react';

const { Option } = Select;
const { Text } = Typography;

type Props = { shippingPoint: PointType, deliveryPoint: PointType, id: number }

export function Application({ shippingPoint, deliveryPoint, id }: Props) {
    const shippingPointList = useAppSelector(state => state.moduleApplicationTransportation.shippingPointList);
    const deliveryPointList = useAppSelector(state => state.moduleApplicationTransportation.deliveryPointList);
    const dispatch = useAppDispatch();

    const selectedApplication = useAppSelector(state => state.moduleApplicationTransportation.selectedApplication);

    return (
        <div
            className={selectedApplication?.id === id ? styles.requestClick : styles.request}
            onClick={() => dispatch(updateSelectedApplication({ id, shippingPoint, deliveryPoint }))}
        >
            <Text className={styles.text}>{id}</Text>

            <Select
                className={styles.select}
                defaultValue={shippingPoint.name}
                onChange={(value) => setTimeout(() => dispatch(updateShippingPointInApplication({ applicationId: id, pointName: value, select: deliveryPoint })), 0) }

                
            >
                {shippingPointList.map((point, index) => {
                    return <Select.Option
                    
                        key={index}
                        value={point.name}>
                        {point.name}
                    </Select.Option>
                })}

            </Select>

            <Select
                className={styles.select}
                defaultValue={deliveryPoint.name}
                onChange={(value) => setTimeout(() => dispatch(updateDeliveryPointInApplication({ applicationId: id, pointName: value, select: shippingPoint })),0)}
            >
                {deliveryPointList.map((point, index) => {
                    return <Select.Option
                        key={index}
                        value={point.name}>
                        {point.name}
                    </Select.Option>
                })}

            </Select>

        </div>)
}
